﻿// <copyright file="ISequence.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager
{
    using System.Threading.Tasks;

    /// <summary>
    /// Provide a mechanism to get <see cref="Int64"/> values from a sequence
    /// </summary>
    public interface ISequence
    {
        /// <summary>
        /// Get the current value of the sequence
        /// </summary>
        /// <returns>The current value of the sequence</returns>
        Task<long> Current();

        /// <summary>
        /// Get the next value of the sequence
        /// </summary>
        /// <returns>The next value of the sequence</returns>
        Task<long> Next();
    }
}
