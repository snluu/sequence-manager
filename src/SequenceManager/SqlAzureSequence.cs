﻿// <copyright file="SqlAzureSequence.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager
{
    using System;
    using System.Data.SqlClient;
    using System.Threading.Tasks;

    /// <summary>
    /// Implementation of <see cref="ISequence"/> using SQL Azure
    /// </summary>
    public class SqlAzureSequence : ISequence
    {
        /// <summary>
        /// The connection string to SQL Azure, set by the constructor
        /// </summary>
        private readonly string connectionString;

        /// <summary>
        /// The SQL table name which contains the sequence value
        /// </summary>
        private readonly string tableName;

        /// <summary>
        /// Offset value being added to the sequence values
        /// </summary>
        private readonly long offset;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlAzureSequence" /> class with offset zero
        /// </summary>
        /// <param name="connectionString">The connection string</param>
        /// <param name="tableName">The table name that contains the sequence value</param>
        public SqlAzureSequence(string connectionString, string tableName)
            : this(connectionString, tableName, offset: 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlAzureSequence" /> class
        /// </summary>
        /// <param name="connectionString">The connection string</param>
        /// <param name="tableName">The table name that contains the sequence value</param>
        /// <param name="offset">The offset being added to the sequence value</param>
        public SqlAzureSequence(string connectionString, string tableName, long offset)
        {
            if (offset < 0)
            {
                throw new ArgumentException("Offset cannot be negative", "offset");
            }

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentException("The connection string is required", "connectionString");
            }

            if (string.IsNullOrWhiteSpace(tableName))
            {
                throw new ArgumentException("The table name is required", "tableName");
            }

            this.connectionString = connectionString;
            this.tableName = tableName;
            this.offset = offset;
        }

        /// <summary>
        /// Get the current value of the sequence
        /// </summary>
        /// <returns>The current value of the sequence</returns>
        public async Task<long> Current()
        {
            string query = string.Format("select top 1 SequenceValue from [{0}]", this.tableName);
            return await this.ExecuteInt64Query(query) + this.offset;
        }

        /// <summary>
        /// Get the next value of the sequence
        /// </summary>
        /// <returns>The next value of the sequence</returns>
        public async Task<long> Next()
        {
            string query = string.Format("update [{0}] set SequenceValue=SequenceValue+1 output Inserted.SequenceValue", this.tableName);
            return await this.ExecuteInt64Query(query) + this.offset;
        }

        /// <summary>
        /// Execute a query in SQL Azure which is expected to return a BIGINT field
        /// </summary>
        /// <param name="query">The query string</param>
        /// <returns>The BIGINT value returned from the database</returns>
        private async Task<long> ExecuteInt64Query(string query)
        {
            using (SqlConnection conn = new SqlConnection(this.connectionString))
            {
                await conn.OpenAsync();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;

                    using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                    {
                        if (!await reader.ReadAsync())
                        {
                            throw new ApplicationException("SQL query not returning data as expected");
                        }

                        return reader.GetInt64(0);
                    }
                }
            }
        }
    }
}
