﻿// <copyright file="SqlAzureSequenceTests.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test cases for <see cref="SqlAzureSequence"/>
    /// </summary>
    [TestClass]
    public class SqlAzureSequenceTests
    {
        /// <summary>
        /// Instance of the test sequence
        /// </summary>
        private ISequence sequence;

        /// <summary>
        /// The table name being used by the test sequence
        /// </summary>
        private string tableName;

        /// <summary>
        /// Initialize the test case
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            this.tableName = "TestSequence_" + Guid.NewGuid().ToString();
            TestHelper.CreateSqlSequenceTable(this.tableName);

            this.sequence = new SqlAzureSequence(TestGlobals.SqlConnectionString, this.tableName);
        }

        /// <summary>
        /// Clean up the test case
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            TestHelper.DropSqlSequenceTable(this.tableName);
        }

        /// <summary>
        /// Test getting the current value of the sequence
        /// </summary>
        [TestMethod]
        public void TestGetCurrentValue()
        {
            Assert.AreEqual(0L, this.sequence.Current().Result, "New sequence should start at zero");
            Assert.AreEqual(1L, this.sequence.Next().Result, "Increasing a new sequence should return one");

            // for the next 100 iterations, the value should not change
            for (int i = 0; i < 100; i++)
            {
                Assert.AreEqual(1L, this.sequence.Current().Result);
            }
        }

        /// <summary>
        /// Test getting the next value of the sequence
        /// </summary>
        [TestMethod]
        public void TestNext()
        {
            for (int i = 1; i < 100; i++)
            {
                Assert.AreEqual((long)i, this.sequence.Next().Result);
                Assert.AreEqual((long)i, this.sequence.Current().Result);
            }
        }

        /// <summary>
        /// Test calling Next() from multiple thread to make sure the numbers are not skipped
        /// </summary>
        [TestMethod]
        public void TestGetNextValueMultiThreadFinalCount()
        {
            const int ThreadCount = 10;
            const int IterationPerThread = 100;

            Thread[] threads = new Thread[ThreadCount];

            for (int i = 0; i < ThreadCount; i++)
            {
                threads[i] = new Thread(() =>
                {
                    for (int x = 0; x < IterationPerThread; x++)
                    {
                        this.sequence.Next().Wait();
                    }
                });
            }

            foreach (Thread t in threads)
            {
                t.Start();
            }

            foreach (Thread t in threads)
            {
                t.Join();
            }

            long currentValue = this.sequence.Current().Result;
            Assert.AreEqual((long)(ThreadCount * IterationPerThread), currentValue);
        }

        /// <summary>
        /// Test calling Next() from multiple threads and make sure that all the numbers returned are unique
        /// </summary>
        [TestMethod]
        public void TestGetNextValueMultiThreadUniqueness()
        {
            const int ThreadCount = 10;
            const int IterationPerThread = 100;

            Thread[] threads = new Thread[ThreadCount];
            bool[] numberSeen = new bool[(ThreadCount * IterationPerThread) + 1];

            for (int i = 0; i < ThreadCount; i++)
            {
                threads[i] = new Thread(() =>
                {
                    List<long> numbers = new List<long>(capacity: IterationPerThread);

                    for (int x = 0; x < IterationPerThread; x++)
                    {
                        numbers.Add(this.sequence.Next().Result);
                    }

                    lock (numberSeen)
                    {
                        foreach (long n in numbers)
                        {
                            Assert.IsFalse(numberSeen[n], "Found duplicate in numbers");
                            numberSeen[n] = true;
                        }
                    }
                });
            }

            foreach (Thread t in threads)
            {
                t.Start();
            }

            foreach (Thread t in threads)
            {
                t.Join();
            }
        }
    }
}
