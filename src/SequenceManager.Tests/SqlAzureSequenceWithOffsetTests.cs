﻿// <copyright file="SqlAzureSequenceWithOffsetTests.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test cases for <see cref="SqlAzureSequence" /> related to offset
    /// </summary>
    [TestClass]
    public class SqlAzureSequenceWithOffsetTests
    {
        /// <summary>
        /// Instance of the test sequence
        /// </summary>
        private ISequence sequence;

        /// <summary>
        /// The table name being used by the test sequence
        /// </summary>
        private string tableName;

        /// <summary>
        /// The offset provided to the sequence
        /// </summary>
        private long offset;

        /// <summary>
        /// Initialize the test case
        /// </summary>
        [TestInitialize]
        public void Init()
        {
            this.tableName = "TestSequence_" + Guid.NewGuid().ToString();
            TestHelper.CreateSqlSequenceTable(this.tableName);

            this.offset = new Random().Next(0, int.MaxValue);
            this.sequence = new SqlAzureSequence(TestGlobals.SqlConnectionString, this.tableName, this.offset);
        }

        /// <summary>
        /// Clean up the test case
        /// </summary>
        [TestCleanup]
        public void CleanUp()
        {
            TestHelper.DropSqlSequenceTable(this.tableName);
        }

        /// <summary>
        /// Test that the offset is being added to the value returned
        /// </summary>
        [TestMethod]
        public void TestSequenceOffset()
        {
            for (int i = 0; i < 100; i++)
            {
                Assert.AreEqual(this.offset + i, this.sequence.Current().Result);
                Assert.AreEqual(this.offset + i + 1, this.sequence.Next().Result);
            }
        }
    }
}
