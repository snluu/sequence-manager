﻿// <copyright file="TestHelper.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager.Tests
{
    using System.Data.SqlClient;

    /// <summary>
    /// This class contains helper methods for test cases
    /// </summary>
    public static class TestHelper
    {
        /// <summary>
        /// Create a table in the database and get it ready to be used for a sequence
        /// </summary>
        /// <param name="tableName">Name of the table being created</param>
        public static void CreateSqlSequenceTable(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(TestGlobals.SqlConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = string.Format("create table [{0}] (SequenceValue bigint primary key not null default 0)", tableName);
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = string.Format("insert into [{0}] (SequenceValue) values (0)", tableName);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Drop a table from the database
        /// </summary>
        /// <param name="tableName">Name of the table being dropped</param>
        public static void DropSqlSequenceTable(string tableName)
        {
            using (SqlConnection conn = new SqlConnection(TestGlobals.SqlConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = string.Format("drop table [{0}]", tableName);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
