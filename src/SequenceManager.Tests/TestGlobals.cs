﻿// <copyright file="TestGlobals.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>
// <license>
//    This software may be modified and distributed under the terms of the MIT license. See the LICENSE.txt file for details.
// </license>

namespace SequenceManager.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Contain global variables for the test project
    /// </summary>
    [TestClass]
    public static class TestGlobals
    {
        /// <summary>
        /// Gets the SQL connection string being used for the test cases
        /// </summary>
        public static string SqlConnectionString { get; private set; }

        /// <summary>
        /// One time initialization before the tests in this assembly start
        /// </summary>
        /// <param name="context">The test context</param>
        [AssemblyInitialize]
        public static void AssemblyInit(TestContext context)
        {
            SqlConnectionString =
                Environment.GetEnvironmentVariable("TestSqlConnectionString") ??
                "server=localhost;database=SequenceManager_Test;trusted_connection=true";
        }
    }
}
